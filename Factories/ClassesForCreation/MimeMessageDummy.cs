﻿using Factories.Interfaces;
using MimeKit;

namespace Factories.ClassesForCreation
{
        class MimeMessageDummy : IEmailMessage<MimeMessage>
        {
            private MimeMessage _message = new MimeMessage();

            public MimeMessage Message
            {
                get => _message;
                set => _message = value;
            }
        }
    }

    class MimeMessageDummy
    {
    }