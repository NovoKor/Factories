﻿using Factories.Interfaces;
using System;
using System.Collections.Generic;

namespace Factories
{ 
 class EmailSender<TMessageType>
{
    public EmailSender(
            IMailSenderFactory<TMessageType> senderFactory,
            IMessageFactory<TMessageType> messageFactory
        )
    {
        _senderFactory = senderFactory
            ?? throw new ArgumentNullException(nameof(senderFactory));

        _messageFactory = messageFactory
            ?? throw new ArgumentNullException(nameof(messageFactory));
    }

    private readonly IMailSenderFactory<TMessageType> _senderFactory;
    private readonly IMessageFactory<TMessageType> _messageFactory;

    public void SendEmail(List<string> addressees, string subj, string message)
    {
        foreach (string email in addressees)
        {
            _senderFactory
                .Create()
                .SendMessage(_messageFactory.Create("olyanovokovskaya@ya.ru", email, subj, message));
        }
    }
}
}
