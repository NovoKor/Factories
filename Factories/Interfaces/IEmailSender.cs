﻿namespace Factories.Interfaces
    {
        public interface IEmailSender<TMessageType>
        {
            void SendMessage(IEmailMessage<TMessageType> message);
        }
    }