﻿namespace Factories.Interfaces
{
    interface IMailSenderFactory<TMessageType>
    {
        IEmailSender<TMessageType> Create();
    }
}
