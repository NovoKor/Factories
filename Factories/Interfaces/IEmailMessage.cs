﻿namespace Factories.Interfaces
{
    public interface IEmailMessage<TMessageType>
    {
        TMessageType Message { get; set; }
    }
}