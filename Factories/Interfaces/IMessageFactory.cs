﻿namespace Factories.Interfaces
{
    interface IMessageFactory<TMessageType>
    {
        IEmailMessage<TMessageType> Create();
        IEmailMessage<TMessageType> Create(string from, string to, string subj, string body);
    }
}
