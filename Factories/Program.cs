﻿using Microsoft.Extensions.DependencyInjection;
using MimeKit;
using System;
using System.Collections.Generic;

namespace Factories
{
    class Program
    {
        private static readonly IServiceProvider container;

        static Program()
        {
            container = ServicesConfigurator.BuildContainer();
        }

        static void Main(string[] args)
        {
            List<string> addressees =
              new List<string>() {
                  "o.novokovskaya@gmail.com",
                  "olyanovokovskaya@ya.ru"
                };

            container.GetService<EmailSender<MimeMessage>>()
                .SendEmail(addressees, "Привет!", "Лабораторная работа готова!");
        }
    }
}