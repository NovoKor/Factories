﻿using Factories.ClassesForCreation;
using Factories.Factories;
using Factories.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using MimeKit;
using System;

namespace Factories
{
    class ServicesConfigurator
    {
  public static IServiceProvider BuildContainer()
            => new ServicesConfigurator().services.BuildServiceProvider();

    private ServicesConfigurator()
    {
        RegisterServices();
    }

class MimeMessageDummy : IEmailMessage<MimeMessage>
        {
            private MimeMessage _message = new MimeMessage();

            public MimeMessage Message
            {
                get => _message;
                set => _message = value;
            }

        }
            private readonly IServiceCollection services = new ServiceCollection();

    private void RegisterServices()
    {
            services.AddSingleton<IEmailSender<MimeMessage>, MailKitMessageSender>();

            services.AddTransient<IEmailMessage<MimeMessage>, MimeMessageDummy>();

            services.AddSingleton<IMessageFactory<MimeMessage>, MimeMessageFactory>();

            services.AddSingleton<IMailSenderFactory<MimeMessage>, MimeMessagesSenderFactory>();

            services.AddSingleton<EmailSender<MimeMessage>>();
        }
}
}
