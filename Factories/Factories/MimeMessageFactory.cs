﻿using Factories.Interfaces;
using MimeKit;
using MimeKit.Text;
using System;

namespace Factories.Factories
{
    class MimeMessageFactory : IMessageFactory<MimeMessage>
    {

        public MimeMessageFactory(IEmailMessage<MimeMessage> blankMessage)
        {
            _blankMessage = blankMessage
                ?? throw new ArgumentNullException(nameof(blankMessage)); ;
        }

        private readonly IEmailMessage<MimeMessage> _blankMessage;

        public IEmailMessage<MimeMessage> Create()
        => _blankMessage;

        public IEmailMessage<MimeMessage> Create(string from, string to, string subj, string body)
        {
            MimeMessage message = new MimeMessage();

            message.From.Add(InternetAddress.Parse(from));
            message.To.Add(InternetAddress.Parse(to));

            message.Subject = subj;
            message.Body = new TextPart(TextFormat.Plain) { Text = body };

            _blankMessage.Message = message;

            return _blankMessage;
        }
    }
}
