﻿using Factories.Interfaces;
using MailKit.Net.Smtp;
using MimeKit;

namespace Factories.Factories
{
    class MailKitMessageSender : IEmailSender<MimeMessage>
    {
        private static SmtpClient client = new SmtpClient();

        public static SmtpClient Client { get => client; set => client = value; }

        static MailKitMessageSender()
        {
            Client.Connect("smtp.yandex.com", 465, true);

            Client.Authenticate("olyanovokovskaya", password: "z280397!");
        }

        public void SendMessage(IEmailMessage<MimeMessage> message)
        {
            Client.Send(message.Message);
        }

        ~MailKitMessageSender()
        {
            Client.Disconnect(true);
        }
    }
}
