﻿using Factories.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using MimeKit;
using System;

namespace Factories.Factories
{
    class MimeMessagesSenderFactory : IMailSenderFactory<MimeMessage>
    {
        private readonly IServiceProvider _container;

        public MimeMessagesSenderFactory(IServiceProvider container)
        {
            _container = container
                ?? throw new ArgumentNullException(nameof(container));
        }

        public IEmailSender<MimeMessage> Create()
        {
            return _container.GetService<IEmailSender<MimeMessage>>();
        }
    }
}
